<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../style.css">
    <title>Game Mô Phỏng - STEAMVN Shop</title>
</head>
<script src="../../Javascript/app.js"></script>
<body>
	<?php include("../../Pages/header.php"); ?>
	<?php
		$search = isset($_GET['textbox_search']) ? $_GET['textbox_search'] : "";
        include '../../connect_db.php';
        $item_per_page = !empty($_GET['per_page'])?$_GET['per_page']:12;
        $current_page = !empty($_GET['page'])?$_GET['page']:1; //Trang hiện tại
        $offset = ($current_page - 1) * $item_per_page;
		if($search){
			$products = mysqli_query($con, "SELECT * FROM `product` WHERE `name` LIKE '%".$search."%' ORDER BY `id` ASC  LIMIT " .$item_per_page . " OFFSET " . $offset);
			$totalRecords = mysqli_query($con, "SELECT * FROM `product` WHERE `name` LIKE '%".$search."%'");
		}
		else{
			$products = mysqli_query($con, "SELECT `product`.* FROM `product`,`categories` WHERE `product`.`id_categories` = `categories`.`id_categories` and `categories`.`name` like '%Mô%'  ORDER BY `id` ASC  LIMIT " . $item_per_page . " OFFSET " . $offset);
        	$totalRecords = mysqli_query($con, "SELECT * FROM `product`");
		}
        
        $totalRecords = $totalRecords->num_rows;
        $totalPages = ceil($totalRecords / $item_per_page);
		$categories = mysqli_query($con, "SELECT * FROM `categories` WHERE `categories`.`name` like '%Mô%'");
      ?>
    <div class="container-categories">
    <div class="main-categories">
        <div class="header-category">
             <div>
                <a class="text_back" href="../../index.html">&lt; Trang chủ</a>
             </div>
             <div>
				 <?php
					while ($row1 = mysqli_fetch_array($categories)) {
				?>
                <h3 class="title-categories"><?php echo $row1["name"]; ?></h3>
                <p class="mota-categories"><?php echo $row1["content"]; ?></p>
				 <?php }?>
            </div>
            <div class="tieude-danhsach">
                <a class="text-tieude" href="mophong.html"> CÁC GAME MÔ PHỎNG</a>
            </div>
			
            <div class="item-categories">
				<?php
                	while ($row = mysqli_fetch_array($products)) {
                 ?>
                <div class="card">
                    <div class="card_heart">
                        <div class="heart-like-button"></div>
                    </div>
                    <div class="card_cart">
                        <img class="card-icon" src="../../Images/cart-regular-24.png">
                    </div>
                    <div class="card_ima">
                        <img class="img5" src="<?= '../../admin/'.$row['image']; ?>">
                        <div class="middle">
                            <a class="text" href="?Cocainit">Xem chi tiết</a>
                      </div>
                    </div>
                    <div class="card_title">
                        <?= $row['name'] ?>
                    </div>
                    <div class="card_price"><?= number_format($row['price'], 0, ",", ".").'$' ?></div>
                    <div class="card_dlc">
                        <h3 class"text_card_dlc">Option&#58;</h3>
                        <span class="menu-link menu-active" data-menu="1" style="padding:3px 20px !important;">Full</span>
                        <span class="menu-link" data-menu="2">Without DLC</span>
                    </div>
                    <div class="card_action">
                        <button class="open-modal-btn">BUY NOW</button>
                        <button>Xem Chi Tiết</button>
                    </div>
                    
                </div>
				<?php } ?>
            </div>
      </div>
    </div>
</div>
<?php 
	include('modalpopup.php'); 
	include('../../Pages/footer.php');
?>

<script src="../../Javascript/app.js"></script>
<script src="../../Javascript/categories.js"></script>
</body>
</html>