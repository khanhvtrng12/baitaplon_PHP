-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 13, 2022 at 01:26 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tieuluansteamvn`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id_categories` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  PRIMARY KEY (`id_categories`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id_categories`, `name`, `image`, `content`, `created_time`, `last_updated`) VALUES
(16, 'Äá»‘i KhÃ¡ng', 'images/img_232101.png', 'ChÆ¡i trÃ² chÆ¡i chiáº¿n Ä‘áº¥u trÃªn STEAMVN SHOP. Chiáº¿n Ä‘áº¥u cho tá»›i vÃ²ng cuá»‘i cÃ¹ng trong nhá»¯ng tráº­n chiáº¿n 1 Ä‘áº¥u 1 kháº¯c nghiá»‡t cho Ä‘áº¿n khi báº¡n lÃ  ngÆ°á»i cuá»‘i cÃ¹ng. ChÆ¡i cÃ¡c trÃ² chÆ¡i nhÆ° Wrestle Jump, Whack Your Ex hay Drunken Wrestlers trong má»¥c trÃ² chÆ¡i Ä‘Ã¡nh nhau', 1657637139, 1657637139),
(17, 'Kinh dá»‹', 'images/ghost-creppy.png', 'Game kinh dá»‹ lÃ  thá»ƒ loáº¡i dÃ¹ng Ä‘á»ƒ hÃ¹ dá»a ngÆ°á»i chÆ¡i nÃªn thÆ°á»ng chá»‰ chÆ¡i Ä‘Æ°á»£c 1 ngÆ°á»i. Náº¿u chÆ¡i cÃ¹ng báº¡n bÃ¨ thÃ¬ cÃ²n gÃ¬ lÃ  sá»£ ná»¯a. Tuy nhiÃªn nhá»¯ng tá»±a game kinh dá»‹ chÆ¡i cÃ¹ng báº¡n bÃ¨ trÃªn Ä‘iá»‡n thoáº¡i Ä‘Ã£ ra Ä‘á»i.', 1657637166, 1657637166),
(18, 'MÃ´ phá»ng', 'images/mophong.png', 'Game mÃ´ phá»ng lÃ  1 dáº¡ng trÃ² chÆ¡i video Ä‘a dáº¡ng. CÃ¡c hoáº¡t Ä‘á»™ng trong tháº¿ giá»›i thá»±c sáº½ Ä‘Æ°á»£c mÃ´ phá»ng láº¡i 1 cÃ¡ch cháº·t cháº½ trong game. Game mÃ´ phá»ng sáº½ cá»‘ gáº¯ng sao chÃ©p cÃ¡c hoáº¡t Ä‘á»™ng khÃ¡c nhau tá»« Ä‘á»i thá»±c cho cÃ¡c má»¥c Ä‘Ã­ch khÃ¡c nhau.', 1657637198, 1657637198),
(19, 'Chiáº¿n thuáº­t', 'images/chienthuat.png', 'Game chiáº¿n thuáº­t hay cÃ²n gá»i lÃ  game chiáº¿n lÆ°á»£c/trÃ² chÆ¡i Ä‘iá»‡n tá»­ chiáº¿n lÆ°á»£c, viáº¿t táº¯t lÃ  SLG. LÃ  thá»ƒ loáº¡i video game mÃ  ngÆ°á»i chÆ¡i cáº§n táº­p trung nhiá»u vÃ o ká»¹ nÄƒng thiáº¿t káº¿, vÃ  tÆ° duy nháº±m giÃ nh Ä‘Æ°á»£c chiáº¿n tháº¯ng.', 1657637225, 1657637225),
(20, 'Sinh tá»“n', 'images/Pubg_Logo.png', 'Game Sinh tá»“n lÃ  má»™t nhÃ¡nh trong thá»ƒ loáº¡i game HÃ nh Ä‘á»™ng, phiÃªu lÆ°u. Cá»‘t truyá»‡n trong nhÃ¡nh game nÃ y thÆ°á»ng xoay quanh nhá»¯ng tai náº¡n, tháº£m há»a Ä‘áº·t báº¡n vÃ o tÃ¬nh huá»‘ng pháº£i chiáº¿n Ä‘áº¥u Ä‘á»ƒ sá»‘ng sÃ³t Ä‘em Ä‘áº¿n cho báº¡n nhiá»u tráº£i nghiá»‡m má»›i máº» vÃ  thÃº vá»‹.', 1657637254, 1657637254);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `content` text NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  `id_categories` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_categories` (`id_categories`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `image`, `price`, `content`, `created_time`, `last_updated`, `id_categories`) VALUES
(25, 'Tekken7', 'images/Tekken7.jpeg', 99, '', 1657641544, 1657641544, 16),
(26, 'Street Fighter V', 'images/Street_Fighter_V.jpg', 99, '', 1657641587, 1657641587, 16),
(27, 'NieR: Automata', 'images/automata.png', 99, '', 1657641635, 1657641635, 16),
(28, 'OUTLAST', 'images/OUTLAST.png', 99, '', 1657641677, 1657641677, 17),
(29, 'OUTLAST 2', 'images/OUTLAST_2.jpeg', 99, '`', 1657641706, 1657641714, 17),
(30, 'The Evil Within', 'images/The_Evil_Within.jpeg', 99, '', 1657641744, 1657641744, 17),
(31, 'Summer in Mara', 'images/Summer_in_Mara.png', 99, '', 1657641864, 1657641864, 18),
(32, 'Stardew Valley', 'images/Stardew_Valley.png', 99, '', 1657641888, 1657641888, 18),
(33, 'Flight Simulator', 'images/Flight_Simulator.png', 99, '', 1657641913, 1657641913, 18),
(34, 'Clash of Clans', 'images/Clash_of_Clans.png', 99, '', 1657641944, 1657641944, 19),
(35, 'Plants vs. Zombies', 'images/Plants_vs._Zombies.png', 99, '', 1657641961, 1657641961, 19),
(36, 'Bloons Tower Defense', 'images/Bloons_Tower_Defense.png', 99, '', 1657641981, 1657641981, 19),
(37, 'ARK Survival Evolved', 'images/ARK_Survival_Evolved.png', 99, '', 1657642014, 1657642014, 20),
(38, 'The Forest', 'images/The_Forest.png', 99, '', 1657642031, 1657642031, 20),
(39, 'Raft', 'images/Raft.png', 99, '', 1657642048, 1657642048, 20);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `birthday` int(11) NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `fullname`, `password`, `birthday`, `created_time`, `last_updated`) VALUES
(1, 'admin', 'khanhcute', '12345678', 123, 123, 1553185530);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_id_categories` FOREIGN KEY (`id_categories`) REFERENCES `categories` (`id_categories`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
