<?php
include 'header.php';
if (!empty($_SESSION['current_user'])) {
    $item_per_page = (!empty($_GET['per_page'])) ? $_GET['per_page'] : 10;
    $current_page = (!empty($_GET['page'])) ? $_GET['page'] : 1;
    $offset = ($current_page - 1) * $item_per_page;
    $totalRecords1 = mysqli_query($con, "SELECT * FROM `categories`");
    $totalRecords1 = $totalRecords1->num_rows;
    $totalPages = ceil($totalRecords1 / $item_per_page);
    $categories = mysqli_query($con, "SELECT * FROM `categories` ORDER BY `id_categories` DESC LIMIT " . $item_per_page . " OFFSET " . $offset);
    mysqli_close($con);
    ?>
    <div class="main-content">
        <h1>Danh sách thể loại</h1>
        <div class="product-items">
            <div class="buttons">
                <button class="custom-btn btn-15" onclick="location.href='categories_editing.php'" style="width:131px!important;">Thêm thể loại</a>
            </div>
            <ul>
                <li class="product-item-heading">
                    <div class="product-prop product-img">Ảnh</div>
                    <div class="product-prop product-name" style="width:193px;">Tên thể loại</div>
                    <div class="product-prop product-button">
                        Xóa
                    </div>
                    <div class="product-prop product-button">
                        Sửa
                    </div>
                    <div class="product-prop product-button">
                        Copy
                    </div>
					<div class="product-prop product-time" style="width:109px;">Ngày Tạo</div>
                    <div class="product-prop product-time" style="width:108px;">Ngày cập nhật</div>
                    <div class="clear-both"></div>
                </li>
                <?php
                while ($row = mysqli_fetch_array($categories)) {
                    ?>
                    <li>
                        <div class="product-prop product-img"><img src="<?= $row['image'] ?>" alt="<?= $row['name'] ?>" title="<?= $row['name'] ?>" /></div>
                        <div class="product-prop product-name"><?= $row['name'] ?></div>
                        <div class="product-prop product-button">
                            <a href="categories_delete.php?id=<?= $row['id_categories'] ?>">Xóa</a>
                        </div>
                        <div class="product-prop product-button">
                            <a href="categories_editing.php?id=<?= $row['id_categories'] ?>">Sửa</a>
                        </div>
                        <div class="product-prop product-button">
                            <a href="categories_editing.php?id=<?= $row['id_categories'] ?>&task=copy">Copy</a>
                        </div>
						
						<div class="product-prop product-time"><?= date('d/m/Y H:i', $row['created_time']) ?></div>
                        <div class="product-prop product-time"><?= date('d/m/Y H:i', $row['last_updated']) ?></div>
                        <div class="clear-both"></div>
                    </li>
                <?php } ?>
            </ul>
            <?php
            include 'pagination.php';
            ?>
            <div class="clear-both"></div>
        </div>
    </div>
    <?php
}
include 'footer.php';
?>